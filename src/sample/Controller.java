package sample;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import sample.model.Datasource;
import sample.model.People;

import javax.xml.transform.Result;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private TableView<People> table;
    @FXML
    private TableColumn<People,String> colid;
    @FXML
    private TableColumn<People,String> fnameid;
    @FXML
    private TableColumn<People,String> lnameid;
    @FXML
    private TableColumn<People,String> emailid;
    @FXML
    private TableColumn<People,String> dobid;
    private ObservableList<People> data;
    private Datasource dc;
    private Connection con;
    private String sql = "SELECT id,fname,lname,email,dob FROM people ";
    @FXML
    private Label status;


    @FXML
   public void loadButton(ActionEvent event) throws SQLException{
       Datasource datasource = new Datasource();
      datasource.createConnection();
       status.setText("Connection done!");
        try{

           Connection conn = datasource.createConnection();
            this.data = FXCollections.observableArrayList();

            ResultSet result = conn.createStatement().executeQuery(sql);

            while(result.next()){                   //check anything has in the table
                this.data.add(new People(result.getString(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5)));
            }

        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
            //display data to table
        this.colid.setCellValueFactory(new PropertyValueFactory<People,String>("id"));      // the id is coming from People class
        this.fnameid.setCellValueFactory(new PropertyValueFactory<People,String>("fname"));
        this.lnameid.setCellValueFactory(new PropertyValueFactory<People,String>("lname"));
        this.emailid.setCellValueFactory(new PropertyValueFactory<People,String>("email"));
        this.dobid.setCellValueFactory(new PropertyValueFactory<People,String>("dob"));

        this.table.setItems(null);
        this.table.setItems(this.data);


   }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.dc = new Datasource();
    }

    @FXML
    public void exit(){
        Platform.exit();
    }
}
