package sample.model;

import java.sql.*;


public class Datasource {
    public static final String url = "jdbc:sqlite:C:\\Users\\achanda\\IdeaProjects\\SqliteDBToTableView\\people.db";
   // public static final String sqconn = "jdbc:sqlite::people.db";
   // public static final String sqconn1 = "jdbc:sqlite::people";


    public static final String Table_People = "people";
    public static final String Col_Id ="id";
    public static final String Col_Fname ="fname";
    public static final String Col_Lname ="lname";
    public static final String Col_Email ="email";
    public static final String Col_DOB ="dob";




    public Connection createConnection() throws SQLException{

        try {
            Class.forName("org.sqlite.JDBC");
            Connection con = DriverManager.getConnection(url);
             return con;



        }catch(SQLException | ClassNotFoundException e){
            System.out.println("Connection not established");
        }

        return null;
    }

}
